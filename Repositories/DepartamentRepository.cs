﻿using Backend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Backend.Repositories
{
    public class DepartamentRepository : IRepository<Departament>
    {
        private Base1Context Context;
        
        public DepartamentRepository()
        {
            Context = new Base1Context();
        }

        public bool Delete(int id)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"DELETE FROM departaments WHERE departaments.id={id};", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }

        public IList<Departament> GetAll()
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                List<Departament> list = new List<Departament>();
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT * FROM departaments;", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Departament()
                        {
                            id = reader.GetInt32("id"),
                            name = reader.GetString("service_name")
                        });
                    }
                }
                return list;
            }
        }

        public Departament GetOne(int id)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                List<Departament> list = new List<Departament>();
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT * FROM departaments WHERE departaments.id={id};", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return null;
                    Departament dep = new Departament()
                    {
                        id = reader.GetInt32("id"),
                        name = reader.GetString("service_name")
                    };
                    return dep;
                } 
            }
        }

        public bool Post(Departament item)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"INSERT INTO departaments(service_name) VALUES('{item.name}');", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }

        public bool Put(int id, Departament item)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"UPDATE departaments SET service_name='{item.name}' WHERE departaments.id={id};", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }
    }
}
