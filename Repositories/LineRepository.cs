﻿using Backend.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class LineRepository
    {
        private Base1Context Context;

        public LineRepository()
        {
            Context = new Base1Context();
        }


        public int Post(RegistrationTime item)
        {
            var response = -1;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT COUNT(*) as amount from registrations where registrations.visit_time>'{item.time.Date} {item.time.ToLocalTime()}' AND registrations.status=0 AND registrations.departament_id={item.departament_id}", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return response;
                    response = reader.GetInt32("amount");
                }
            }
            return response;
        }

    }
}
