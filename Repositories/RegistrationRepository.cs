﻿using Backend.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class RegistrationRepository
    {
        private Base1Context Context;

        public RegistrationRepository()
        {
            Context = new Base1Context();
        }

        public bool Delete(int id)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"DELETE FROM registrations WHERE registrations.id={id};", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }

        public IList<Registration> GetAll()
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                List<Registration> list = new List<Registration>();
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT * FROM registrations;", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Registration()
                        {
                            id = reader.GetString("id"),
                            visit_time = reader.GetDateTime("visit_time"),
                            name = reader.GetString("name"),
                            surname = reader.GetString("surname"),
                            departament_id = reader.GetInt32("departament_id"),
                            user_id = reader.GetInt32("user_id")
                        });
                    }
                }
                return list;
            }
        }

        public Registration GetOne(int id)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT * FROM registrations WHERE registrations.id='{id}';", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return null;
                    Registration reg = new Registration()
                    {
                        id = reader.GetString("id"),
                        visit_time = reader.GetDateTime("visit_time"),
                        name = reader.GetString("name"),
                        status = reader.GetInt32("status"),
                        surname = reader.GetString("surname"),
                        departament_id = reader.GetInt32("departament_id"),
                        user_id = reader.GetInt32("user_id")
                    };
                    return reg;
                }
            }
        }

        public bool Post(Registration item)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"INSERT INTO registrations(id, visit_time, departament_id, user_id, name, surname) VALUES('{item.id}', '{item.visit_time}', '{item.departament_id}', '{item.user_id}', '{item.name}', '{item.surname}');", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }

        public int Put(RegistrationTime item)
        {
            var response = -1;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT COUNT(*) as amount from registrations where registrations.visit_time>'{item.time.Date} {item.time.ToLocalTime()}' AND registrations.status=0; AND registrations.departament_id={item.departament_id}", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return response;
                    response = reader.GetInt32("amount");
                }
            }
            return response;
        }

        public DateTime Latest(Registration item)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"select * from registrations where registrations.departament_id={item.departament_id} and registrations.user_id={item.user_id} order by registrations.visit_time DESC limit 1;", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return DateTime.Now;
                    return reader.GetDateTime("visit_time").AddMinutes(5);
                }
            }
        }

        internal bool CheckForId(string item)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"select * from registrations where registrations.id='{item}';", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return true;
                    return false;
                }
            }
        }


    }
}
