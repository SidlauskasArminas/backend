﻿using Backend.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private Base1Context Context;

        public UserRepository()
        {
            Context = new Base1Context();
        }

        public bool Delete(int id)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"DELETE FROM users WHERE users.id={id};", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }

        public IList<User> GetAll()
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                List<User> list = new List<User>();
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT * FROM users;", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new User()
                        {
                            id = reader.GetInt32("id"),
                            name = reader.GetString("name"),
                            surname = reader.GetString("surname"),
                            email = reader.GetString("email"),
                            departament_id = reader.GetInt32("departament_id")
                        });
                    }
                }
                return list;
            }
        }

        public User GetOne(int id)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT * FROM users WHERE users.id={id};", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return null;
                    User usr = new User()
                    {
                        id = reader.GetInt32("id"),
                        username = reader.GetString("username"),
                        name = reader.GetString("name"),
                        surname = reader.GetString("surname"),
                        email = reader.GetString("email"),
                        departament_id = reader.GetInt32("departament_id")
                    };
                    return usr;
                }
            }
        }

        public bool Post(User item)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"INSERT INTO users(username, password, name, surname, email, departament_id) VALUES('{item.username}','{item.password}','{item.name}','{item.surname}','{item.email}','{item.departament_id}');", conn);
                try
                {
                    response = (cmd.ExecuteNonQuery() == 1) ? true : false;
                }
                catch(Exception e)
                {
                    return false;
                }
                
            }
            return response;
        }

        public bool Put(int id, User item)
        {
            var response = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"UPDATE users SET username='{item.username}', password='{item.password}', name='{item.password}', surname='{item.surname}', email='{item.email}', departament_id='{item.departament_id}' WHERE users.id={id};", conn);
                response = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return response;
        }
    }
}
