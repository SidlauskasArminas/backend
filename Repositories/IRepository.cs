﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    interface IRepository<T>
    {
        public IList<T> GetAll();
        public T GetOne(int id);
        public bool Post(T item);
        public bool Delete(int id);
        public bool Put(int id, T item);
    }
}
