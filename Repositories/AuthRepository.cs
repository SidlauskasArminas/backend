﻿using Backend.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class AuthRepository
    {
        private Base1Context Context;

        public AuthRepository()
        {
            Context = new Base1Context();
        }

        public Auth Post(UserLogin user)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"select * from users where users.username='{user.username}' and users.password='{user.password}';", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    if (!reader.HasRows)
                        return null;
                    Auth auth = new Auth()
                    {
                        id = reader.GetInt32("id"),
                        username = reader.GetString("username")
                    };
                    return auth;
                }
            }
        }
    }
}
