﻿using Backend.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Repositories
{
    public class TicketRepository
    {
        private Base1Context Context;

        public TicketRepository()
        {
            Context = new Base1Context();
        }

        public IList<Ticket> Get(int id)
        {
            using (MySqlConnection conn = Context.GetConnection())
            {
                List<Ticket> list = new List<Ticket>();
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"SELECT registrations.id, registrations.visit_time, registrations.name, registrations.surname, departaments.service_name as departament, registrations.status  FROM registrations left join departaments on registrations.departament_id=departaments.id left join users on registrations.user_id=users.id where users.id = {id} and (registrations.status=0 or registrations.status=1) ORDER BY registrations.visit_time ASC;", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Ticket()
                        {
                            id = reader.GetInt32("id"),
                            visit_time = reader.GetDateTime("visit_time"),
                            name = reader.GetString("name"),
                            surname = reader.GetString("surname"),
                            departament = reader.GetString("departament"),
                            status = reader.GetInt32("status")
                        });
                    }
                }
                return list;
            }
        }

        public bool Put(int id, int status)
        {
            var check = false;
            using (MySqlConnection conn = Context.GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand($"UPDATE registrations SET status={status} WHERE registrations.id={id} ;", conn);
                check = (cmd.ExecuteNonQuery() == 1) ? true : false;
            }
            return check;
        }
    }
}
