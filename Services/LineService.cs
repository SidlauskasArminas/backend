﻿using Backend.Entities;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class LineService
    {
        LineRepository _repository;

        public LineService()
        {
            _repository = new LineRepository();
        }

        public int Post(RegistrationTime item)
        {
            return _repository.Post(item);

        }
    }
}
