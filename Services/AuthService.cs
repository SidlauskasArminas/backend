﻿using Backend.Entities;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class AuthService
    {
        private IJWTManager JWTManager;
        private readonly AuthRepository _repository;

        public AuthService(IJWTManager JWTManager)
        {
            this.JWTManager = JWTManager;
            this._repository = new AuthRepository();
        }
        public Auth Post(UserLogin user)
        {
            user.password = ComputeSha256Hash(user.password);
            var auth = _repository.Post(user);
            if (auth == null)
                return null;
            var token = JWTManager.Authenticate(user.username);
            auth.accessToken = token;
            return auth;
        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
