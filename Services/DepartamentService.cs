﻿using Backend.Entities;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class DepartamentService : IService<Departament>
    {
        private readonly IRepository<Departament> _repository;
        public DepartamentService()
        {
            _repository = new DepartamentRepository();
        }
        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public IList<Departament> GetAll()
        {
            return _repository.GetAll();
        }

        public Departament GetOne(int id)
        {
            return _repository.GetOne(id);
        }

        public bool Post(Departament item)
        {
            return _repository.Post(item);
        }

        public bool Put(int id, Departament item)
        {
            return _repository.Put(id, item);
        }
    }
}
