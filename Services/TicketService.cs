﻿using Backend.Entities;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class TicketService
    {
        TicketRepository _repository;

        public TicketService()
        {
            _repository = new TicketRepository();
        }

        public IList<Ticket> Get(int id)
        {
            var item = _repository.Get(id);
            return item;
        }

        public bool Put(int id, int status)
        {
            var item = _repository.Put(id, status);
            return item;
        }
    }
}
