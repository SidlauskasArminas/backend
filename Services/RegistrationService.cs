﻿using Backend.Entities;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class RegistrationService
    {
        private readonly RegistrationRepository _repository;
        public RegistrationService()
        {
            _repository = new RegistrationRepository();
        }
        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public IList<Registration> GetAll()
        {
            return _repository.GetAll();
        }

        public Registration GetOne(int id)
        {
            return _repository.GetOne(id);
        }

        public string Post(Registration item)
        {
            item.status = 0;
            bool fitting = false;
            while(!fitting)
            {
                item.id = generate_id(5);
                fitting = _repository.CheckForId(item.id);
            }
            item.visit_time = _repository.Latest(item);
            var diff = DateTime.Now - item.visit_time;
            if (diff.TotalMinutes > 5.0)
                item.visit_time = DateTime.Now;
            var check = _repository.Post(item);
            if (!check)
                return null;
            return item.id;
        }

        public int Put(RegistrationTime item)
        {
            return _repository.Put(item);
        }

        private string generate_id(int length)
        {
            Random rand = new Random();
            string id = "";
            for (int i = 0; i < length; i++)
            {
                id += rand.Next(0, 9);
            }
            return id;
        }
    }
}
