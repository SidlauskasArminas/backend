﻿using Backend.Entities;
using Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class UserService
    {
        private readonly IRepository<User> _repository;
        private readonly IRepository<Departament> _repositoryDep;
        public UserService()
        {
            _repository = new UserRepository();
            _repositoryDep = new DepartamentRepository();
        }
        public bool Delete(int id)
        {
            return _repository.Delete(id);
        }

        public IList<User> GetAll()
        {
            return _repository.GetAll();
        }

        public Profile GetOne(int id)
        {
            var user = _repository.GetOne(id);
            if (user == null)
                return null;
            var dep = _repositoryDep.GetOne(user.departament_id);
            Profile profile = new Profile()
            {
                id = user.id,
                username = user.username,
                name = user.name,
                surname = user.surname,
                email = user.email,
                departament_id = dep.id,
                departament_name = dep.name
            };
            return profile;
        }

        public bool Post(User item)
        {
            item.password = ComputeSha256Hash(item.password);
            return _repository.Post(item);
        }

        public bool Put(int id, User item)
        {
            return _repository.Put(id, item);
        }

        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
