﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    
    [Route("api/departament")]
    [ApiController]
    public class DepartamentController : ControllerBase
    {
        private readonly IService<Departament> _service;
        public DepartamentController()
        {
            _service = new DepartamentService();
        }

        // GET: api/<Service_Dep>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            var departaments = _service.GetAll();
            return Ok(departaments);
        }

        // GET api/<Service_Dep>/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            var departament = _service.GetOne(id);
            if (departament == null)
                return NotFound($"Departament with id: {id} was not found");
            return Ok(departament);
        }

        // POST api/<Service_Dep>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post(Departament departament)
        {
            var result = _service.Post(departament);
            if (!result)
                return BadRequest("Bad request, departament not placed");
            return Ok("Departament posted");
        }

        // PUT api/<Service_Dep>/5
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Put(int id, Departament departament)
        {
            var result = _service.Put(id, departament);
            if (!result)
                return BadRequest("Bad request, departament not updated");
            return Ok("Departament updated");
        }

        // DELETE api/<Service_Dep>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            var result = _service.Delete(id);
            if (!result)
                return BadRequest($"Departament with id = {id} not found");
            return Ok($"Departament with id = {id} removed");
        }
    }
}
