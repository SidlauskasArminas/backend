﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IJWTManager JWTManager;
        private AuthService _service;

        public AuthController(IJWTManager JWTManager)
        {
            this.JWTManager = JWTManager;
            this._service = new AuthService(JWTManager);
        }

        // POST api/<AuthentificateController>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult Authentificate([FromBody] UserLogin user)
        {
            var token = _service.Post(user);
            if (token == null)
                return Unauthorized();
            return Ok(token);
        }
    }
}
