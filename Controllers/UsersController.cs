﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _service;
        public UsersController()
        {
            _service = new UserService();
        }



        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            var item = _service.GetAll();
            return Ok(item);
        }


        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            var item = _service.GetOne(id);
            if (item == null)
                return NotFound($"User with id: {id} was not found");
            return Ok(item);
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post(User item)
        {
            var result = _service.Post(item);
            if (!result)
                return BadRequest("Bad request, user not placed");
            return Ok("User posted");
        }


        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Put(int id, User item)
        {
            var result = _service.Put(id, item);
            if (!result)
                return BadRequest("Bad request, user not updated");
            return Ok("User updated");
        }


        //[HttpDelete("{id}")]
        //[ProducesResponseType(StatusCodes.Status200OK)]
        //[ProducesResponseType(StatusCodes.Status404NotFound)]
        //public IActionResult Delete(int id)
        //{
        //    var result = _service.Delete(id);
        //    if (!result)
        //        return BadRequest($"User with id = {id} not found");
        //    return Ok($"User with id = {id} removed");
        //}
    }
}
