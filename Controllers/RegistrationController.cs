﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Entities;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Backend.Controllers
{
    [Route("api/registration")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly RegistrationService _service;
        public RegistrationController()
        {
            _service = new RegistrationService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Get()
        {
            var item = _service.GetAll();
            return Ok(item);
        }


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            var item = _service.GetOne(id);
            if (item == null)
                return NotFound($"Registration with id: {id} was not found");
            return Ok(item);
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post(Registration item)
        {
            var id = _service.Post(item);
            if (id == null)
                return BadRequest("Bad request, departament registration not placed");
            return Ok(id);
        }



        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Delete(int id)
        {
            var result = _service.Delete(id);
            if (!result)
                return BadRequest($"Departament registration with id = {id} not found");
            return Ok($"Departament registration with id = {id} removed");
        }
    }
}
