﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Entities
{
    public class Registration
    {
        public string id { get; set; }
        public DateTime visit_time { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int status { get; set; }
        public int departament_id { get; set; }
        public int user_id { get; set; }
    }
}
