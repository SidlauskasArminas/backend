﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Entities
{
    public class Auth
    {
        public int id { get; set; }
        public string username { get; set; }
        public string accessToken { get; set; }
    }
}
