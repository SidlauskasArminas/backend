﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Entities
{
    public class RegistrationTime
    {
        public DateTime time { get; set; }
        public int departament_id { get; set; }
    }
}
