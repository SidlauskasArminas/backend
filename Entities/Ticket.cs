﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Entities
{
    public class Ticket
    {
        public int id { get; set; }
        public DateTime visit_time { get; set; }
        public int status { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string departament { get; set; }
    }
}
