﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Entities
{
    public class Departament
    {
        public int id { get; set; } 
        public string name { get; set; }
    }
}
