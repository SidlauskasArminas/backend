-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: base1
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departaments`
--

DROP TABLE IF EXISTS `departaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departaments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `service_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departaments`
--

LOCK TABLES `departaments` WRITE;
/*!40000 ALTER TABLE `departaments` DISABLE KEYS */;
INSERT INTO `departaments` VALUES (3,'Postal Office'),(5,'Bank'),(6,'Outpatient clinic');
/*!40000 ALTER TABLE `departaments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registrations` (
  `id` varchar(10) NOT NULL,
  `visit_time` datetime DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `departament_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `fk_departament` (`departament_id`),
  KEY `fk_user` (`user_id`),
  CONSTRAINT `fk_departament` FOREIGN KEY (`departament_id`) REFERENCES `departaments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrations`
--

LOCK TABLES `registrations` WRITE;
/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;
INSERT INTO `registrations` VALUES ('14734','2021-03-29 09:33:34',0,'Jonas','Karčiauskas',5,16),('16280','2021-03-28 17:19:42',2,'Petras','Jonaitis',5,16),('16516','2021-03-28 19:37:36',2,'Arminas','Šidlauskas',5,16),('18348','2021-03-29 09:13:34',2,'arvydas','Sabonis ',5,16),('22377','2021-03-28 18:00:27',2,'Julius','Siauliys',5,16),('25773','2021-03-29 09:43:34',0,'Kazimieras','Balčys',5,16),('27121','2021-03-29 09:23:34',2,'petras','nepetras',5,16),('33576','2021-03-28 22:36:08',2,'Henrikas','Šukys',5,16),('38020','2021-03-28 17:09:42',2,'Julius','Julius',5,16),('38288','2021-03-29 09:03:34',-1,'opa','veik',5,16),('40372','2021-03-28 22:31:08',2,'Ignas','Balciunas',5,16),('43011','2021-03-29 09:38:34',0,'Janina','Rudienė',5,16),('44846','2021-03-29 08:18:15',0,'Tomas','Pečinskas',5,20),('48543','2021-03-29 07:44:49',0,'Tomas','Jaknys',5,18),('51026','2021-03-28 19:32:36',2,'Tomas','Tomaitis',5,16),('51575','2021-03-28 17:04:42',2,'Bronislavas','Tulpietis',5,16),('52070','2021-03-29 09:18:34',2,'Tomas ','Žitkus',5,16),('65340','2021-03-28 21:29:20',2,'Jonas','Miklovas',5,16),('73142','2021-03-28 17:14:42',2,'Tomas','Jonaitis',5,16),('74512','2021-03-28 17:24:42',2,'Ignas','Balciunas',5,16),('74531','2021-03-28 20:06:52',2,'Tomas','Laimutis',5,16),('78528','2021-03-29 09:28:34',1,'Tomas','Pančys',5,16),('78785','2021-03-28 22:41:08',2,'Darude','Sandstorm',5,16);
/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `departament_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dep` (`departament_id`),
  CONSTRAINT `fk_dep` FOREIGN KEY (`departament_id`) REFERENCES `departaments` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (16,'arminas','cd92c1a761343c971fcebce0b13a24b335e944b90354a65c3bcc0785efb35c0f','Arminas','Sidlauskas','arminas@gmail.com',5),(17,'jonas','bf74e4a280affafbdf6692bc6a9f3d66b03094fbfb4a91589bc7fd6b32664fdb','Jonas','Jarkaitis','jonukas@gmail.com',3),(18,'petras','6205cdb5c34a252f7c1a1206a594da810584cac75f4cc252794ff8a3a45e5bb6','Petras','Petraitis','petras@gmail.com',5),(19,'alfredas','a54c5afe1d2914b41d758d9a49583a4f5a345f77a815850ed67a8f47d417fb9f','Alfredas','Pranskaitis','alfredas@gmail.com',6),(20,'kamile','0c9344d6b82b72e53f2e9a785cac4b172d268a6f1d132687cf0f12fbcca9ee1e','Kamilė','Kvėdaraitė','kamile@gmail.com',5);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-29  9:35:44
